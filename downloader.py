from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class DownloaderClass:
    def __init__(self):
        print("Starting web driver...")
        self.driver = webdriver.Firefox()
        self.parent_window = None
        self.child_window = None

    def __del__(self):
        print("Closing web driver...")
        for window_id in self.driver.window_handles:
            print("Closing {} window handler".format(window_id))
            self.driver.switch_to.window(window_id)
            self.driver.close()

    def open_webpage(self, url):
        self.driver.get(url)
        assert "selenium test" in self.driver.title
        element = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.ID, "open"))
        )
        self.parent_window = self.driver.current_window_handle
        print("The parent window handle: {}".format(self.parent_window))
        time.sleep(5)  # ez csak bemutató miatt van itt

    def fill_text_input(self, css_selector, text_input):
        print("Filling text: {} , {}".format(css_selector, text_input))
        element = self.driver.find_element(By.CSS_SELECTOR, css_selector)
        element.send_keys(text_input)
        time.sleep(5)

    def click_on_link_open_new_window(self, link_text):
        print("Searching for clickable linke: {}".format(link_text))
        element = self.driver.find_element(By.PARTIAL_LINK_TEXT, link_text)
        element.click()
        for window_id in self.driver.window_handles:
            if window_id != self.parent_window:
                print("New child window id: {}".format(window_id))
                self.child_window = window_id
        time.sleep(5)

    def close_child_window(self):
        if self.child_window is None:
            print("No child window to close")
        else:
            print("Closing {} child...".format(self.child_window))
            self.driver.switch_to.window(self.child_window)
            self.driver.close()
        # switching back to parent window
        self.driver.switch_to.window(self.parent_window)
        time.sleep(1)


if __name__ == "__main__":
    tester = DownloaderClass()
    tester.open_webpage("http://localhost:5000/stest")
    tester.fill_text_input("input#fname", "kitöltés")
    tester.click_on_link_open_new_window("Teszt li")
    tester.close_child_window()
    tester.fill_text_input("input#end", "ez a vége")
