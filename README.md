## webpage_automation

#### Requirements:  
Python 3+  
https://www.python.org/

Selenium  
https://selenium-python.readthedocs.io/installation.html

Selenium web drivers:  
For Firefox:  
https://github.com/mozilla/geckodriver/releases

For Chrome:  
https://sites.google.com/a/chromium.org/chromedriver/downloads

Download a web driver and make it available to the environment variables

#### Installation commands:
Create a folder and download the scripts to it  
then execute the following commands:

`python -m venv tutorial`

`tutorial\Scripts\activate.bat`

`python setup.py install`

`python downloader.py`
